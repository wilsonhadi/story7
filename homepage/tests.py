from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from django.urls import resolve
from .views import *
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time 

# Create your tests here.
class test(TestCase):

    def test_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_is_url_index_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_template_index_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_function_confirmation(self):
        found = resolve('/confirmation/')
        self.assertEqual(found.func, confirmation)

    def test_is_url_confirmation_exist(self):
        response = Client().get('/confirmation/')
        self.assertEqual(response.status_code,200)
    
    def test_template_confirmation_used(self):
        response = Client().get('/confirmation/')
        self.assertTemplateUsed(response, 'confirmation.html')

    # ----------------------------- models and forms -------------------------------

    def test_create_status(self):
        new = Status.objects.create(name = 'test', status = 'test status')
        self.assertTrue(isinstance(new, Status))
        self.assertTrue(new.__str__(), new.status)
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)
    
    def test_form_index_is_valid(self):
        response = self.client.post('', data={'name' : 'Name', 'status' : 'Status'})
        response_content = response.content.decode()
        self.assertNotIn(response_content, 'Status')
        self.assertNotIn(response_content, 'Name')

    def test_form_confirmation_is_valid(self):
        response = self.client.post('/confirmation/', data={'name' : 'Name', 'status' : 'Status'})
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)
        self.assertEqual(response.status_code, 302)
        response_content = response.content.decode()
        self.assertIn(response_content, 'Status')
        self.assertIn(response_content, 'Name')

    def test_display_status(self):
        response = Client().post('/', {'name': 'test name', 'status': 'test status'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirmation.html')

class Story7FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

    def tearDown(self):
        self.driver.quit()

    def test_home_title(self):
        self.driver.get(self.live_server_url)
        self.assertIn("Hello", self.driver.title)

    def test_index_greeting(self):
        self.driver.get(self.live_server_url)
        self.assertIn("Hello, how are you?", self.driver.page_source)

    def test_fill_in_form(self):
        self.driver.get(self.live_server_url)
        name = self.driver.find_element_by_name('name')
        name.send_keys("test name")
        status = self.driver.find_element_by_name('status')
        status.send_keys("test status")
        button = self.driver.find_element_by_id('submit')
        button.click()
        self.assertIn("Confirmation", self.driver.page_source)

    def test_fill_in_form_and_save(self):
        self.driver.get(self.live_server_url)
        name = self.driver.find_element_by_name('name')
        name.send_keys("test name")
        status = self.driver.find_element_by_name('status')
        status.send_keys("test status")
        button = self.driver.find_element_by_id('submit')
        button.click()
        button = self.driver.find_element_by_name('save')
        button.click()
        self.assertIn("Hello", self.driver.title)

    def test_fill_in_form_and_not_save(self):
        self.driver.get(self.live_server_url)
        name = self.driver.find_element_by_name('name')
        name.send_keys("test name")
        status = self.driver.find_element_by_name('status')
        status.send_keys("test status")
        button = self.driver.find_element_by_id('submit')
        button.click()
        button = self.driver.find_element_by_name('back')
        button.click()
        self.assertIn("Hello", self.driver.title)

    def test_change_color(self):
        self.driver.get(self.live_server_url)
        name = self.driver.find_element_by_name('name')
        name.send_keys("test name")
        status = self.driver.find_element_by_name('status')
        status.send_keys("test status")
        button = self.driver.find_element_by_id('submit')
        button.click()
        button = self.driver.find_element_by_name('save')
        button.click()
        element = self.driver.find_element_by_name("challenge")
        color = element.value_of_css_property("color")
        self.assertEqual(color, "rgba(33, 37, 41, 1)")
        button = self.driver.find_element_by_name('challengeButton')
        button.click()
        element = self.driver.find_element_by_name("challenge")
        color = element.value_of_css_property("color")
        self.assertEqual(color, "rgba(36, 51, 112, 1)")

